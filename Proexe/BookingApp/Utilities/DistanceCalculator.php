<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


class DistanceCalculator {

	/**
	 * @param array  $from
	 * @param array  $to
	 * @param string $unit - m, km
	 *
	 * @return mixed
	 */
	public function calculate( $from, $to, $unit = 'm' ) {

        //Calculate distance from latitude and longitude
	    $rad = M_PI / 180;
        $theta = $from[1] - $to[1];
        $dist = sin($from[0] * $rad)
            * sin($to[0] * $rad) +  cos($from[0] * $rad)
            * cos($to[0] * $rad) * cos($theta * $rad);

        return ceil((acos($dist) / $rad * 60 *  (($unit == 'km') ? 1.853 : 1853)));
	}

	/**
	 * @param array $from
	 * @param array $offices
	 *
	 * @return array
	 */
	public function findClosestOffice( $from, $offices ) {

	    //SELECT a.id AS from_office, b.id AS to_office,
        //   111.111 *
        //    DEGREES(ACOS(LEAST(1.0, COS(RADIANS(a.lat))
        //         * COS(RADIANS(b.lat))
        //         * COS(RADIANS(a.lng - b.lng))
        //         + SIN(RADIANS(a.lat))
        //         * SIN(RADIANS(b.lat))))) AS distance_in_km
        //  FROM offices AS a
        //  JOIN offices AS b ON a.id <> b.id
        // WHERE a.id = 3 AND b.id = 7

	    $allDistances = [];

		foreach($offices as $office){

            $allDistances[$office['name']] = $this->calculate([ $from[0], $from[1] ], [ $office['lat'], $office['lng'] ], 'km');
        }
		asort($allDistances);

		return array_keys($allDistances)[0].' ('.array_values($allDistances)[0].')';
	}
}
