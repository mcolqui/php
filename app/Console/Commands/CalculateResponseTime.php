<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Proexe\BookingApp\Bookings\Models\BookingModel;
use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;
use Proexe\BookingApp\Utilities\ResponseTimeCalculator;

class CalculateResponseTime extends Command implements ResponseTimeCalculatorInterface
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bookingApp:calculateResponseTime';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates response time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function calculate($bookingDateTime, $responseDateTime, $officeHours)
    {
        $first_date = new \DateTime($bookingDateTime);
        $first_date_day = date_format(new \DateTime($bookingDateTime), "D");

        $second_date = new \DateTime($responseDateTime);
        $second_date_day = date_format(new \DateTime($responseDateTime), "D");

        $interval = $first_date->diff($second_date);

        $weekdays = array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");

        $bookingDay = array_keys($weekdays, $first_date_day);
        $responseDay = array_keys($weekdays, $second_date_day);

        if(($first_date_day == $second_date_day) && $officeHours[$bookingDay[0]]['isClosed'] == false){

            return "difference ".$interval->d." days ".$interval->h." hours ".$interval->i." minutes ";
        }
        elseif($first_date_day != $second_date_day){

            if($officeHours[$bookingDay[0]]['isClosed'] == false){

            }
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $bookings = BookingModel::with('office')->get()->toArray();

        foreach($bookings as $b){


            $this->line('created_at = '.$b['created_at'].'('.date_format(new \DateTime($b['created_at']), "D").')');
            $this->line('updated_at = '.$b['updated_at'].'('.date_format(new \DateTime($b['updated_at']), "D").')');
            $this->line('Opening hours: ');
            $this->line('Response time will be: ');
            $this->line('Difference: '.$this->calculate($b['created_at'], $b['updated_at'], $b['office']['office_hours']));
            $this->line('');
            $this->line('Response time will be: ');
            $this->line('');
            $this->line('------------------------------------');
            $this->line('');
        }

	    //Use ResponseTimeCalculator class for all calculations
	    //You can use $this->line() to write out any info to console
    }
}
